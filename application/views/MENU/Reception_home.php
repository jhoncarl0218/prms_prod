<ul class="nav navbar-top-links navbar-right">
                <li class="dropdown"></li>
                <!-- /.dropdown -->
                
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Admin
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?php echo site_url('Welcome/userp')?>"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="<?php echo site_url('Welcome/userp')?>"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                       <li class="divider"></li>
                        <li><a href="<?php echo site_url('log_in/index')?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                      
                        <li>
                            <a href="<?php echo site_url('D_home/home')?>"><i class="fa fa-home fa-3x"></i> Home</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-table fa-3x"></i> Daily Appointments<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                 <li>
                                    <a href="<?php echo site_url('daily_appointments/table1')?>">Appointments</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('daily_appointments/table1')?>">List of Patient</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="<?php echo site_url('List_user/table')?>"><i class="fa fa-table fa-3x"></i> List of User</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('Add_user/add_user_view')?>"><i class="fa fa-edit fa-3x"></i> Register User</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('User_profile/userp')?>"><i class="fa fa-user fa-3x"></i> User Profile</a>
                        </li>

                      
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>