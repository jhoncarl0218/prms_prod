<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PRMS</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url()?>asset/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url()?>asset/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url()?>asset/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url()?>asset/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>
            <!-- /.navbar-header -->

            <?php $this->load->view('MENU/menu_navigator'); ?>
       
    <form id="userForm" class="form-horizontal" action="<?php echo base_url()?>index.php/Patient_form/pForm" role="form-group" method="post">
        <div id="page-wrapper">
            <div class="row">
                <div class="">
                    <h2 class="page-header">Registration Form</h2>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4> Register New Patient</h4>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="">
                                    <form >
                                         <div class="form-group col-md-4 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> First Name</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="first_name" class="form-control " style = "margin-left: 1px;" placeholder="first name" required="">
                                        </div>
                                         <div class="form-group col-md-4 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Middle Name</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="middle_name" class="form-control " style = "margin-left: 1px;" placeholder="middle name" required="">
                                        </div>
                                        <div class="form-group col-md-4 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Last Name</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="last_name" class="form-control " style = "margin-left: 1px;" placeholder="last name" required="">
                                        </div> 
                                          <div class="form-group col-md-3 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Date of Birth</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="Date_of_Birth" class="form-control " style = "margin-left: 1px;" placeholder="Date of Birth" required="">
                                        </div>
                                        <div class="form-group col-md-6 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label>Address</label>
                                            <textarea type="textarea" name="Address" style = "margin-left: 1px;" placeholder="Address here" class="form-control" required="Address" rows="2"></textarea>
                                        </div>
                                          <label class="col-md-3">
                                                    Gender
                                            </label>
                                                <div class="col-md-3">
                                                    <select name = "Gender" class="form-control" required="Gender">
                                                        <option >Please Select Gender</option>
                                                        <option>Male</option>
                                                        <option>Female</option>
                                                    </select>
                                                </div>            
                                        </div>
                                        <div class="form-group col-md-4 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> NHTS Number</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="nhts_number" class="form-control " style = "margin-left: 1px;" placeholder="card number">
                                        </div>
                                         <div class="form-group col-md-4 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> FourP's Number</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="fourps_number" class="form-control " style = "margin-left: 1px;" placeholder="card number">
                                        </div>
                                        <div>
                                            <label class="col-md-4">
                                                    Remarks
                                            </label>
                                                <div class="col-md-4">
                                                    <select name = "Remarks" class="form-control" required="Status">
                                                        <option >Please Select Remarks</option>
                                                        <option>Member</option>
                                                        <option>Dependent</option>
                                                        <option>None</option>
                                                    </select>
                                                </div>
                                        </div>
                                            <div class="form col-md-12 col-sm-2 col-xs-12">
                                                <button  type="submit" class="btn btn-success" style = "background-color: green;">Submit</button>
                                                <button type="reset" class="btn btn-success" style = "background-color: blue;">Reset</button>
                                            </div>
                                        </form>
                                    </div>
                                </div> 
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>  <!-- /.row -->
        
	</form>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo base_url()?>asset/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url()?>asset/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url()?>asset/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url()?>asset/dist/js/sb-admin-2.js"></script>
     <script src="<?php echo base_url()?>asset/js/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>asset/app/prms.js"></script>
     
</body>

</html>
