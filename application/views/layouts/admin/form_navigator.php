<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PRMS</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url()?>asset/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url()?>asset/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url()?>asset/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url()?>asset/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

            <?php $this->load->view('MENU/menu_navigator'); ?>
       
        <form id="userForm" class="form-horizontal" action="<?php echo base_url()?>index.php/Add_User/add_user" role="form-group" method="post">
        <div id="page-wrapper">
            <div class="row">
                <div class="">
                    <h2 class="page-header">Registration Form</h2>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4> Register New User</h4>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="">
                                    <form >
                                        <div>
                                             <label class="form col-md-12 col-sm-5 col-xs-12"> First Name</label>
                                                <div class="form-group col-md-7 col-sm-5 col-xs-12 ">      
                                            <input type="text" name="first_name" class="form-control" style = "margin-left: 15px;" placeholder="First Name" required>
                                            </div>
                                        </div>

                                         
                                        <label class="form col-md-12 col-sm-5 col-xs-12">Last Name</label>
                                        <div class="form-group col-md-7 col-sm-3 col-xs-12">
                                            
                                            <input type="text" name = "last_name" class="form-control" style = "margin-left: 15px;" placeholder="Last Name" required>
                                        </div >
                                        <div>
									        <label for="Position" required="Position" class="col-md-12">
										            Position
                                            </label>
									    <div class="col-md-7">
										<select name = "Position" class="form-control" re>
											<option>Please Select Position</option>
											<option>Admin</option>
											<option>Doctor</option>
											<option>Receptionist</option>
										</select>
									</div>            
								</div>

                                      
                                        <div class="form-group col-md-6 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Username</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="username" class="form-control " style = "margin-left: 0px;" placeholder="" required="">
                                        </div>
                                        <div class="form-group col-md-6 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Password</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="Password" name="Password" class="form-control " style = "margin-left: 1px;" placeholder="" required="">
                                        </div>
                                       
                                         <div class="form-group col-md-7 col-sm-2 col-xs-12">
                                           <form>
                                                  <input style = "margin-left: 15px;" type="checkbox"  name="Active" value="Active" required=""> Active<br>
                                            </form>
                                           
                                        </div>
                                        
                                    
                                        <div class="form col-md-12 col-sm-2 col-xs-12">
                                            <button  type="submit" class="btn btn-success" onclick="submit" style = "background-color: blue;">Submit</button>
                                            <button type="reset" class="btn btn-success" style = "background-color: red;">Reset</button>
                                    </form>
                                </div>



                               
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        
	</form>
    </div>
       

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo base_url()?>asset/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url()?>asset/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url()?>asset/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url()?>asset/dist/js/sb-admin-2.js"></script>

</body>

</html>
