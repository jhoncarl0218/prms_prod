<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PRMS</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url()?>asset/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url()?>asset/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    
    <!--jQuery UI Css -->
    <link href="<?php echo base_url()?>asset/vendor/jquery-ui/jquery-ui.min.css" rel="stylesheet">

    

    <!-- Custom CSS -->
    <link href="<?php echo base_url()?>asset/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url()?>asset/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>


</head>

<body>
            <!-- /.navbar-header -->

            <?php $this->load->view('MENU/menu_navigator'); ?>

    <form id="userForm" class="form-horizontal" action="#" role="form-group" method="post">
        <div id="page-wrapper">
            <div class="row">
                <div class="">
                    <h2 class="page-header">Daily Appointments</h2>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4> Add Appointment</h4>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="">
                                    <div class="panel-heading">
                                           <h4> Search Patient</h4>
                                        </div>
                                    <form >
                                        <div class="form-group col-md-4 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Patient Name</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input id="appt_patient_id" type="text" name="appt_patient" class="form-control " style = "margin-left: 1px;" placeholder="Patient Name" >
                                        </div>
                                        
                                         <div class="form-group col-md-12 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <center>
                                            <label>Physical Examination</label>
                                            </center>
                                        </div>
                                        <div class="form-group col-md-3 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Height</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="Height" class="form-control " style = "margin-left: 1px;" placeholder="" >
                                        </div>
                                        <div class="form-group col-md-3 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Weight</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="Weight" class="form-control " style = "margin-left: 1px;" placeholder="">
                                        </div>
                                        <div class="form-group col-md-3 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Waistline</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="Waistline" class="form-control " style = "margin-left: 1px;" placeholder="">
                                        </div>
                                        <div class="form-group col-md-3 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Temperature</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="Waistline" class="form-control " style = "margin-left: 1px;" placeholder="">
                                        </div>
                                        <div class="form-group col-md-3 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Pulse Rate</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="Pulse_Rate" class="form-control " style = "margin-left: 1px;" placeholder="">
                                        </div>
                                        <div class="form-group col-md-3 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Respiratory Rate</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="Respiratory_Rate" class="form-control " style = "margin-left: 1px;" placeholder="">
                                        </div>
                                        <div class="form-group col-md-3 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Blood Pressure</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="Height" class="form-control " style = "margin-left: 1px;" placeholder="">
                                        </div>
                                        <div class="form-group col-md-12 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <center>
                                            <label>Appointment Details</label>
                                        </center>
                                        </div>
                                        
                                        <div class="form-group col-md-5 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label>Note Given</label>
                                            <textarea type="textarea" name="not_given" style = "margin-left: 1px;" placeholder="note given here" class="form-control" rows="10"></textarea>
                                        </div>
                                        <div class="form-group col-md-7 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label>Complaint</label>
                                            <textarea type="textarea" name="complaint" style = "margin-left: 1px;" placeholder="complaint here" class="form-control" rows="10"></textarea>
                                        </div>
                                        <!--div class="form-group col-md-12 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <center>
                                            <label>Assessment/Impression</label>
                                        </center>
                                        </div>
                                        <div class="form-group col-md-7 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label>Diagnosis</label>
                                            <textarea type="textarea" name="Diagnosis" style = "margin-left: 1px;" placeholder="Result of Assessment here" class="form-control" rows="10"></textarea>
                                        </div>
                                        <div class="form-group col-md-5 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label>Treatment</label>
                                            <textarea type="textarea" name="Treatment" style = "margin-left: 1px;" placeholder="Treatment Plan here" class="form-control" rows="10"></textarea>
                                        </div>
                                         <!--div style = "margin-left: 430px;" class="col-md-2">
                                            <center>
                                            <label>Status</label>
                                            </center>
                                        <select name = "Status" class="form-control" >
                                            <option>Select Status</option>
                                            <option>Incomplete</option>
                                            <option>Complete</option>
                                        </select>
                                    </div-->   
                                        <div class="form col-md-12 col-sm-2 col-xs-12">
                                            <button  type="submit" class="btn btn-success" onclick="submit" style = "background-color: blue;">Submit</button>
                                            <button type="reset" class="btn btn-success" style = "background-color: red;">Reset</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </form>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo base_url()?>asset/vendor/jquery/jquery.min.js"></script>

    <!-- jQuery UI-->
    <script src="<?php echo base_url()?>asset/vendor/jquery-ui/jquery-ui.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url()?>asset/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url()?>asset/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url()?>asset/dist/js/sb-admin-2.js"></script>
     <script src="<?php echo base_url()?>asset/js/datepicker/bootstrap-datepicker.js"></script>

     <script>
        /*$("#appt_patient_id").on("keyup", function() {
            if (this.value.trim() != '') {

                //var base_url = window.location.origin;
                //console.log(this.value);

                $.ajax({
                    url: "<?=base_url('index.php/patient_search/search_name')?>",
                    type: "post",
                    data: {"name":this.value},
                    success: function(data){
                        $("#appt_patient_id").val(data);
                    },
                    error: function(xmlhttprequest, textstatus, message) {
                        alert(textstatus);
                    }
                });
            }
        });*/

        $(function() {
            
            //populate the source array            
            //dummy
            var searcharrpatient = [];

            $.ajax({
                url: "<?=base_url('index.php/patient_search/get_all')?>",

            }).done(function(data){
                $.each(JSON.parse(data), function(key,value) {
                    var el = {
                        'value':value['Fullname'],
                        'id':value['PatientID']
                    }
                    searcharrpatient.push(el);
                });

                //console.log(searcharrpatient);
            });

            //for the autocomplete 
            $("#appt_patient_id").autocomplete({
                autoFocus: true,
                source: searcharrpatient,
                minLength: 1,//search after one characters
                
                select: function(event,ui){
                    //do something
                    //alert(ui.item ? ui.item.id : '');
                    localStorage.setItem('patientId', ui.item ? ui.item.id : '');
                    //alert(localStorage.getItem('patientId'));

                },
                change: function(event, ui) {
                     if (ui.item == null) {
                        //unset
                        localStorage.removeItem('patientId');
                        }
                },
                response: function(event, ui) {
                    if (ui.content.length === 0) 
                    {
                        //unset
                        localStorage.removeItem('patientId');                    }
                }
            });
        });
     </script>

</body>

</html>
