<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PRMS</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url()?>asset/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url()?>asset/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url()?>asset/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url()?>asset/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>
            <!-- /.navbar-header -->

            <?php $this->load->view('MENU/menu_navigator'); ?>
       
    <form id="userForm" class="form-horizontal" action="<?php echo base_url()?>index.php/Patient_form/pForm" role="form-group" method="post">
        <div id="page-wrapper">
            <div class="row">
                <div class="">
                    <h2 class="page-header">Registration Form</h2>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4> Register New Patient</h4>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="">
                                    <form >
                                         <div class="form-group col-md-4 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> First Name</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="first_name" class="form-control " style = "margin-left: 1px;" placeholder="first name" required="">
                                        </div>
                                         <div class="form-group col-md-4 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Middle Name</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="middle_name" class="form-control " style = "margin-left: 1px;" placeholder="middle name" required="">
                                        </div>
                                        <div class="form-group col-md-4 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Last Name</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="last_name" class="form-control " style = "margin-left: 1px;" placeholder="last name" required="">
                                        </div> 
                                          <div class="form-group col-md-3 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Date of Birth</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="Date_of_Birth" class="form-control " style = "margin-left: 1px;" placeholder="Date of Birth" required="">
                                        </div>
                                        <div class="form-group col-md-6 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label>Address</label>
                                            <textarea type="textarea" name="Address" style = "margin-left: 1px;" placeholder="Address here" class="form-control" required="Address" rows="2"></textarea>
                                        </div>
                                          <label class="col-md-3">
                                                    Gender
                                            </label>
                                                <div class="col-md-3">
                                                    <select name = "Gender" class="form-control" required="Gender">
                                                        <option >Please Select Gender</option>
                                                        <option>Male</option>
                                                        <option>Female</option>
                                                    </select>
                                                </div>            
                                        </div>
                                        <div class="form-group col-md-4 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> NHTS Number</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="nhts_number" class="form-control " style = "margin-left: 1px;" placeholder="card number">
                                        </div>
                                         <div class="form-group col-md-4 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> FourP's Number</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="fourps_number" class="form-control " style = "margin-left: 1px;" placeholder="card number">
                                        </div>
                                        <div>
                                            <label class="col-md-4">
                                                    Remarks
                                            </label>
                                                <div class="col-md-4">
                                                    <select name = "Remarks" class="form-control" required="Status">
                                                        <option >Please Select Remarks</option>
                                                        <option>Member</option>
                                                        <option>Dependent</option>
                                                        <option>None</option>
                                                    </select>
                                                </div>
                                        </div>
                                            <div class="form col-md-12 col-sm-2 col-xs-12">
                                                <button  type="submit" class="btn btn-success" style = "background-color: green;">Submit</button>
                                                <button type="reset" class="btn btn-success" style = "background-color: gray;">Reset</button>
                                            </div>
                                        </form>
                                    </div>
                                </div> 
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>  <!-- /.row -->
        
	</form>

    <form id="userForm" class="form-horizontal" action="#" role="form-group" method="post">
        <div id="page-wrapper">
            <div class="row">
                <div class="">
                    <h2 class="page-header">Daily Appointments</h2>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4> Add Appointment</h4>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="">
                                    <div class="panel-heading">
                                           <h4> Search Patient</h4>
                                        </div>
                                        <form  class="form-horizontal" action="<?php echo base_url()?>index.php/patient_form/searched" method="post">
                                        <div class="form-group input-group col-md-4 col-sm-2 col-xs-12" style="margin: 15px">
                                            <input type="text" name = "txt_search" class="form-control">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </form>
                                    
                                        
                                        <!--div class="form-group col-md-3 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> First Name</label>
                                            <!--label>Text Input with Placeholder</label->
                                            <input type="text" name="first_name" class="form-control " style = "margin-left: 1px;" placeholder="first name" >
                                        </div>
                                        <div class="form-group col-md-3 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Middle Name</label>
                                            <!--label>Text Input with Placeholder</label->
                                            <input type="text" name="middle_name" class="form-control " style = "margin-left: 1px;" placeholder="middle name" >
                                        </div>
                                        <div class="form-group col-md-3 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Last Name</label>
                                            <!--label>Text Input with Placeholder</label->
                                            <input type="text" name="last_name" class="form-control " style = "margin-left: 1px;" placeholder="last name">
                                        </div>
                                        <!--div class="form-group col-md-3 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Date and Time</label>
                                            <input type="Date" name="Date" class="form-control " style = "margin-left: 0px;" placeholder="" required="">
                                            <input type="time" name="time" class="form-control " style = "margin-left: 0px;" placeholder="" required="">
                                            </center>
                                        </div-->
                                         <div class="form-group col-md-12 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <center>
                                            <label>Physical Examination</label>
                                            </center>
                                        </div>
                                       
                                        <div class="form-group col-md-3 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Weight</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="Weight" class="form-control " style = "margin-left: 1px;" placeholder="">
                                        </div>
                                        <div class="form-group col-md-3 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Waistline</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="Waistline" class="form-control " style = "margin-left: 1px;" placeholder="">
                                        </div>
                                        <div class="form-group col-md-3 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Temperature</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="Waistline" class="form-control " style = "margin-left: 1px;" placeholder="">
                                        </div>
                                        <div class="form-group col-md-3 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Pulse Rate</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="Pulse_Rate" class="form-control " style = "margin-left: 1px;" placeholder="">
                                        </div>
                                        <div class="form-group col-md-3 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Respiratory Rate</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="Respiratory_Rate" class="form-control " style = "margin-left: 1px;" placeholder="">
                                        </div>
                                        <div class="form-group col-md-3 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label> Blood Pressure</label>
                                            <!--label>Text Input with Placeholder</label-->
                                            <input type="text" name="Height" class="form-control " style = "margin-left: 1px;" placeholder="">
                                        </div>
                                        <div class="form-group col-md-12 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <center>
                                            <label>Appointment Details</label>
                                        </center>
                                        </div>
                                        
                                        <div class="form-group col-md-5 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label>Note Given</label>
                                            <textarea type="textarea" name="not_given" style = "margin-left: 1px;" placeholder="note given here" class="form-control" rows="10"></textarea>
                                        </div>
                                        <div class="form-group col-md-7 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label>Complaint</label>
                                            <textarea type="textarea" name="complaint" style = "margin-left: 1px;" placeholder="complaint here" class="form-control" rows="10"></textarea>
                                        </div>
                                        <div class="form-group col-md-12 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <center>
                                            <label>Assessment/Impression</label>
                                        </center>
                                        </div>
                                        <div class="form-group col-md-7 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label>Diagnosis</label>
                                            <textarea type="textarea" name="Diagnosis" style = "margin-left: 1px;" placeholder="Result of Assessment here" class="form-control" rows="10"></textarea>
                                        </div>
                                        <div class="form-group col-md-5 col-sm-2 col-xs-12" style = "margin-left: 0px;">
                                            <label>Treatment</label>
                                            <textarea type="textarea" name="Treatment" style = "margin-left: 1px;" placeholder="Treatment Plan here" class="form-control" rows="10"></textarea>
                                        </div>
                                         <div style = "margin-left: 430px;" class="col-md-2">
                                            <center>
                                            <label>Status</label>
                                            </center>
                                        <select name = "Status" class="form-control" >
                                            <option>Select Status</option>
                                            <option>Incomplete</option>
                                            <option>Complete</option>
                                        </select>
                                    </div>   
                                        <div class="form col-md-12 col-sm-2 col-xs-12">
                                            <button  type="submit" class="btn btn-success" onclick="submit" style = "background-color: blue;">Submit</button>
                                            <button type="reset" class="btn btn-success" style = "background-color: red;">Reset</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </form>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo base_url()?>asset/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url()?>asset/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url()?>asset/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url()?>asset/dist/js/sb-admin-2.js"></script>
     <script src="<?php echo base_url()?>asset/js/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>asset/app/prms.js"></script>
     
</body>

</html>
