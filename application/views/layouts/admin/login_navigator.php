<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PRMS</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url()?>asset/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url()?>asset/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url()?>asset/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url()?>asset/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
 
<body background="<?php echo base_url()?>asset/image/doh.jpg" background-repeat="no-repeat">
    <div class="container col-md-12 col-sm-2 col-xs-12">
        <div class="row ">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default col-md-12 col-sm-2 col-xs-12">
                    <div class="panel-heading">
                        <imag src="<?php echo base_url()?>asset/image/doh-logo-2.jpg" alt="doh-logo-2" width="3890" height="1500">
                        <center>
                        <font size="100" color="green">  NAAWAN RHU</font>
                        <h3 >Please Sign In</h3>
                        </center>
                        <div class="text-danger">
                            <?php if (isset($error)) {echo $error; } ?>
                        </div>
                    </div>
                    <div id="body">
                                <?php echo form_open('log_in/login') ?>
                                 <div class="form-group">
                                    <input class="form-control " placeholder="Username" name="username" type="username" required="username" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" required="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button type="Submit" name="login" placeholder="login" class="btn btn-lg btn-success btn-block">Login</button>

                                <?php echo form_close() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url()?>asset/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url()?>asset/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url()?>asset/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url()?>asset/dist/js/sb-admin-2.js"></script>

</body>

</html>
