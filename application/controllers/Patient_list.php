<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patient_list extends CI_Controller {

	
	public function list_patient()
	{
		$this->load->helper('form');

		$this->db->select(' U_id, PatientID, Fname, Mname, Lname, DateOfBirth, Address, Gender, Remarks, NHTS_NUM, FourPs_num');
		$this->db->from('patients');

		$query = $this->db->get();

		$data['patients'] = $query->result();
		$this->load->view('layouts/admin/list_of_patient_navigator', $data);
	}
	
	
	
}
