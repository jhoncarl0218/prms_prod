<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class log_in extends CI_Controller { 
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Login_model');
		$this->load->library('session');
	}
	public function index()
	{
		$this->load->helper('form');
		$this->load->view('layouts/admin/login_navigator');
		//$this->load->model('Login_model');
	}
	public function login()
	{
		if(isset($_POST['login']))
		{
			$username = $this->input->post('username',true);
			$password = $this->input->post('password',true);
			$Login = $this->Login_model->plogin($username,$password);
			$user = count($Login);
			if($user > 0){
				$openlogin = $this->db->get_where('user',array('username'=>$username,'password'=>$password))->row();

					$this->session->set_userdata('userid', $openlogin->U_id);

					if ($openlogin->Position == 'Admin'){
						$this->session->set_userdata('usertype', 'admin');
						redirect('D_home/home');
						# code...
					}elseif ($openlogin->Position == 'Receptionist'){
						$this->session->set_userdata('usertype', 'rec');
						redirect('D_home/home');
						# code...
					}elseif ($openlogin->Position == 'Doctor'){
						$this->session->set_userdata('usertype', 'doc');
						redirect('D_home/home');
						# code...

					}
				}else{
					$data['error'] = 'Username or Password incorrect!';

					$this->load->view('layouts/admin/login_navigator', $data);
			}
		}
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('Welcome/index');
	}

}
