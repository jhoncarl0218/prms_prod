<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patient_search extends CI_Controller {

	public function search_name()
	{

		if(!empty($_POST['name']))
		{
			
			$this->load->model('Patient_model');
			$var = $_POST['name'];
			$name_res = $this->Patient_model->search_name_show_first_only($var);
			$result = $name_res->FULLNAME;
			echo $result;
			return;
		}
		else
		{
			echo "nothing!";
			return;
		}
		
	}

	public function get_all()
	{
		$this->load->model('patient_model');
		
		$records = $this->patient_model->get_all_names_with_id();

		echo json_encode($records);
		return;

	}
	
	
	
}
