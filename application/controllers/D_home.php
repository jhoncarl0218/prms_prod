<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class D_home extends CI_Controller {
	
	public function home()
	{
		$this->load->helper('form');
		$this->load->view('layouts/admin/home_navigator');
	}
	
	
	
}
