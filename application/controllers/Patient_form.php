<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patient_form extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Patient_model');
		//$this->load->model('Nhts_model');
		$this->load->library('session');
	}
	public function pForm_view()
	{
		$this->load->helper('form');
		$this->load->view('layouts/admin/Patient_form_navigator');
	}
	public function pForm()
	{
		$udata['Fname'] = $this->input->post('first_name');
		$udata['Mname'] = $this->input->post('middle_name');
		$udata['Lname'] = $this->input->post('last_name');
		$udata['DateOfBirth'] = $this->input->post('Date_of_Birth');
		$udata['Address'] = $this->input->post('Address');
		$udata['Gender'] = $this->input->post('Gender');
		$udata['Remarks'] = $this->input->post('Remarks');
		$udata['NHTS_NUM'] = $this->input->post('nhts_number');
		$udata['FourPs_num'] = $this->input->post('fourps_number');
		$udata['U_id'] = $this->session->userdata('userid');

		$pat = $this->Patient_model->new_patient($udata);
		$this->load->helper('form');
		$this->load->view('layouts/admin/Patient_form_navigator'); 

		/*$nhts_n = $this->input->post('nhts_number');
		$fourPs_n = $this->input->post('fourps_number');

		if($nhts_n != '')
		{
			$data['patientid'] = $this->get_patientID();
			$data['cardtype'] = 'NHTS';
			$data['number'] = $nhts_n;
			$data['rem'] = $this->input->post('Remarks');

			$this->insert_card($data);

		}

		if ($fourPs_n != '')
		{
			$data['patientid'] = $this->get_patientID();
			$data['cardtype'] = '4Ps';
			$data['number'] = $fourPs_n;
			$data['rem'] = $this->input->post('Remarks');

			$this->insert_card($data);

		}
		
		$pat = $this->Patient_model->new_patient($udata);
		$this->load->helper('form');
		$this->load->view('layouts/admin/Patient_form_navigator'); 
	}
	private function get_patientID()
	{
		$this->db->select_max('PatientID');
		$q = $this->db->get('patients')->row();
		return $q->PatientID;

	}

	private function insert_card($data)
	{
		$record = array(
			'PatientID' => $data['patientid'],
			'Card_type' => $data['cardtype'],
			'Card_number' => $data['number'],
			'Remarks' => $data['rem']
		);

		$this->Nhts_model->insert_Nhts($record);*/
	}
	
}
