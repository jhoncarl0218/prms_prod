<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_user extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('add_user_model');
	}
	public function add_user_view()
	{
		$this->load->helper('form');
		$this->load->view('Layouts/admin/form_navigator'); 
	}
	public function add_user()
	{
		
		$udata['fname'] = $this->input->post('first_name');
		$udata['lname'] = $this->input->post('last_name');
		$udata['Position'] = $this->input->post('Position');
		$udata['status'] = $this->input->post('Active');
		$udata['username'] = $this->input->post('username');
		$udata['password'] = $this->input->post('Password');
		
		$res = $this->add_user_model->insert_new_user($udata);
		$this->load->helper('form');
		$this->load->view('Layouts/admin/form_navigator'); 
	}
}
