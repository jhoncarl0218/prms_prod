<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class List_patients extends CI_Controller {
	
	public function patients_list()
	{
		$this->load->helper('form');

		$this->db->select(' PatientID, Fname, Mname, Mname, DateOfBirth, Address, Gender');
		$this->db->from('patients');

		$query = $this->db->get();

		$data['patients'] = $query->result();

		$this->load->view('layouts/admin/list_patient_navigator', $data);
	}
	
}
