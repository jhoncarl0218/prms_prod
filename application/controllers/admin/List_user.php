<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class List_user extends CI_Controller {
	
	public function user_list()
	{
		$this->load->helper('form');

		$this->db->select('u_id, fname, lname, position, status, username, password');
		$this->db->from('user');

		$query = $this->db->get();

		$data['users'] = $query->result();

		$this->load->view('layouts/admin/list_user_navigator', $data);
	}
	
}
