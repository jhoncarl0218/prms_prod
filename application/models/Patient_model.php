<?php
    class Patient_model extends CI_Model
    {
        public function new_patient($data)
        {
            if($this->db->insert('patients', $data))
            {
                return true;   
            }
        }

        public function search_name_show_first_only($name)
        {
            $this->db->like('FULLNAME', $name, 'after');
            $record = $this->db->get("patients_name");

            return $record->row();
        }

        public function get_all_names_with_id()
        {

            $record = $this->db->get("patients_name");

            return $record->result();

        }
    }
?>
